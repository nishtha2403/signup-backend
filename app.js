const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const userRouter = require('./routes/users');
const { sequelize } = require('./models');

const app = express();
dotenv.config();

const PORT = process.env.PORT || 4000;

app.use(cors({
    origin: "*"
}));

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.listen(PORT, async () => {
    try {
        console.log(`Server started on ${PORT}`);
        await sequelize.sync({ force: false });
        app.use('/user', userRouter);
    } catch(err) {
        console.error(err);
        resizeBy.status(500).json(err);
    }
});