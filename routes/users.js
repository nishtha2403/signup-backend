const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { User }  = require('../models');
const authenticateJWT = require('../auth');

const router = express.Router();
const saltrounds = 10;

router.post('/register', async (req,res) => {
    try {
        const { name, email } = req.body;
        let { password } = req.body;
        password = await bcrypt.hash(password, saltrounds);
        const user = await User.create({ name, email, password });
        return res.json(user);
    } catch(err) {
        console.log(err);
        return res.status(500).send(err);
    }
});

router.post('/login', async (req,res) => {
    try {
        const email = req.body.email;
        const enteredPassword = req.body.password;

        const { password } = await User.findOne({attributes: ['password'],where: { email: email } });
        const isPasswordCorrect = await bcrypt.compare(enteredPassword,password);

        if(isPasswordCorrect){
            const secretkey = process.env.TOKEN_SECRET;
            const refreshSecretKey = process.env.REFRESH_TOKEN_SECRET;
    
            const accessToken = jwt.sign({email}, secretkey, {expiresIn: '2d'});
            const refreshToken = jwt.sign({email},refreshSecretKey);
    
            return res.send({ accessToken,refreshToken });
        } else {
            throw new Error("Password Incorrect");
        }
    } catch (err) {
        if(err.message === "Password Incorrect") {
            return res.status(400).send({msg: err.message });
        }
        return res.status(400).send({msg: "User doesn't exist" });
    }
});

router.post('/refreshtoken', (req,res) => {
    const { refreshToken } = req.body;
    const secretkey = process.env.TOKEN_SECRET;
    const refreshSecretKey = process.env.REFRESH_TOKEN_SECRET;

    if(!refreshToken) {
        return res.sendStatus(401);
    }

    jwt.verify(refreshToken, refreshSecretKey, (err,user) => {
        if(err) {
            return res.sendStatus(403);
        }

        const accessToken = jwt.sign(user, secretkey, {expiresIn: '2d'});

        res.json({
            accessToken
        });
    })

});

router.get('/', authenticateJWT, async (req,res) => {
    try {
        const usersList = await User.findAll({ attributes: ['id','email']});
        return res.json(usersList);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.get('/:id', authenticateJWT, async (req,res) => {
    try {
        const id = req.params.id;
        const usersDetails = await User.findOne({ attributes: ['id','email','name'], where: {id}});
        return res.json(usersDetails);
    } catch(err) {
        console.log(err);
        return res.status(500).json(err);
    }
});

router.put('/update', authenticateJWT, async (req,res) => {
    try {
        const { email: loggedInEmail } = req.user;
        const { name, email } = req.body;
        let { password } = req.body;

        let userUpdateStatus;

        console.log(loggedInEmail);

        if(password) {
            password = await bcrypt(password,saltrounds);
            [ userUpdateStatus ] = await User.update({ name, email, password }, { where: {email: loggedInEmail} });
        } else {
            [ userUpdateStatus ] = await User.update({ name, email }, { where: {email: loggedInEmail} });
        }

        if(userUpdateStatus) {
            res.redirect('/login');
        }

    } catch(err) {
        console.error(err);

        if(err.message === 'Unauthorized') {
            return res.status(401).json({msg:'Unauthorized'});
        }

        return res.status(400).json(err);
    }
});



module.exports = router;